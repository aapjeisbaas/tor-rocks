#!/bin/bash

name=$1
control_port=$2
socks_port=$3

while true; do
  # check if client is fit for duty
  curl -s -m 0.7 --socks5-hostname 127.0.0.1:$socks_port icanhazip.com
  result=$?
  if [ $result -gt 0 ]; then
    # client not responding take it offline in haproxy
    echo "disable server tors/$name" | socat stdio /app/haproxysock
    # request new tor connection
    printf "AUTHENTICATE \"\"\r\nSIGNAL NEWNYM\r\nEOF" | nc -w 1 127.0.0.1 $control_port
  else
    # client is ok make sure it is online
    echo "enable server tors/$name" | socat stdio /app/haproxysock
  fi
  sleep 1
done
