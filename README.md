This docker machine uses multiple tor clients behind a haproxy instance to balance connections.
It will take a few env vars like:
  - amount of threads
  - check tor client for connection
  - public location to check latency
  - socks port
  - haproxy webinterface port

## How to use:

```
docker pull registry.gitlab.com/aapjeisbaas/tor-rocks:latest
docker run -d -p 8080:8080 -p 3128:3128 registry.gitlab.com/aapjeisbaas/tor-rocks:latest
```

Socks port: `3128`
Haproxy web port: `8080`

Check if it's working:
```
while true; do curl -m 1 --socks5-hostname 127.0.0.1:3128 icanhazip.com; sleep 0.5 ;done
```
Or open a webbrowser: [http://127.0.0.1:8080/haproxy?stats](http://127.0.0.1:8080/haproxy?stats)

![Google Analytics](https://www.google-analytics.com/collect?v=1&tid=UA-48206675-1&cid=555&aip=1&t=event&ec=repo&ea=view&dp=gitlab%2Ftor-rocks%2FREADME.md&dt=tor-rocks)
