#!/bin/bash

while true; do
  
  # get running tor clients
  CLIENTS=$(ps faux | grep 'tor ' | grep -ve 'grep' | sed 's/.*--ControlPort\ //g; s/\ .*SocksPort\ /-/g; s/\ .*--DataDirectory//g; s/data\///g; s/\ /-/g;')
  
  # check every tor client
  for client in $CLIENTS; do
    control_port=$(echo $client |awk -F '-' '{print $1}')
    socks_port=$(echo $client |awk -F '-' '{print $2}')
    name=$(echo $client |awk -F '-' '{print $3}')
    echo "Name: $name socks:$socks_port crtl:$control_port"
  
    # check if client is fit for duty
    curl -s -m 0.7 --socks5-hostname 127.0.0.1:$socks_port icanhazip.com
    result=$?
    if [ $result -gt 0 ]; then
      # client not responding take it offline in haproxy
      echo "disable server tors/$name" | socat stdio /app/haproxysock
      # request new tor connection
      printf "AUTHENTICATE \"\"\r\nSIGNAL NEWNYM\r\nEOF" | nc -w 1 127.0.0.1 $control_port
    else
      # client is ok make sure it is online
      echo "enable server tors/$name" | socat stdio /app/haproxysock
    fi
  done
done
