# Use an official Python runtime as a parent image
FROM haproxy:latest

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN apt-get update
RUN apt-get install socat netcat curl tor vim procps -y

# Open the balanced socks port
EXPOSE 3128

# Define environment variable
ENV THREADS 20

# Run start.sh when the container launches
CMD ["/app/start.sh"]

